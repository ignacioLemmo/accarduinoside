// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

#include "DHT.h"
#include <SPI.h>
#include <Ethernet.h>
#define DHTPIN 5     // what digital pin we're connected to

  byte mac[] = {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
  };
  //IP DE RED LOCAL
  IPAddress ip(192, 168, 0, 31);
  //IP DEL SERVIDOR NODE LOCAL
  IPAddress server(192,168,0, 3);
// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);

int deviceID= 44;

EthernetClient client; 

void setup() {
  Serial.begin(9600);

    setupEthernet();
  
  Serial.println("DHTxx test!");

  dht.begin();

      if (client.connected()) {
     String data = "CONECT/";
     data.concat(deviceID); 
     client.print(data);
                   
      Serial.println("Conectado!");
        
        delay(1000);
      }
      
      else{
        
         Serial.println("CLIENTE NO DISPONIBLE!"); 
           
       }
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

 String data = "TEMP/";
     data.concat(h);
     data.concat("/");
     data.concat(t);
     
     client.print(data); 
     delay(100);
  
}
  void setupEthernet()
  {
    Serial.println("Setting up Ethernet...");
    // start the Ethernet connection:
    Ethernet.begin(mac, ip);
    Serial.print("My IP address: ");
    Serial.println(Ethernet.localIP());
    delay(1000);
  
    client.connect(server, 8888);
    
    delay(1000);
  
  }
