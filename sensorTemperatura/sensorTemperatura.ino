#include <DHT.h>



  /*
    Telnet client
  
   This sketch connects to a a telnet server (http://www.google.com)
   using an Arduino Wiznet Ethernet shield.  You'll need a telnet server
   to test this with.
   Processing's ChatServer example (part of the network library) works well,
   running on port 10002. It can be found as part of the examples
   in the Processing application, available at
   http://processing.org/
  
   Circuit:
   * Ethernet shield attached to pins 10, 11, 12, 13
  
   created 14 Sep 2010
   modified 9 Apr 2012
   by Tom Igoe
  
   */
  
#include <SPI.h>
#include <Ethernet.h>

  //#include <TextFinder.h>
  byte mac[] = {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
  };
  //IP DE RED LOCAL
  IPAddress ip(192, 168, 0, 31);
  //IP DEL SERVIDOR NODE LOCAL
  IPAddress server(192,168,0, 3);
  
  //ID DE DISPOSITIVO OBTENIDO DE CUANDO SE CREA EL DISPOSITIVO
  int deviceID= 44;
  int pinTemp=5;

DHT dht11(pinTemp);
  
  EthernetClient client; 
  //TextFinder  finder( client );
  void setup() {
    
    Serial.begin(9600);
  
   setupEthernet();
  
    if (client.connected()) {
     String data = "CONECT/";
     data.concat(deviceID); 
     client.print(data);
                   
      Serial.println("Conectado!");
        
        delay(1000);
      }
      
      else{
        
         Serial.println("CLIENTE NO DISPONIBLE!"); 
           
       }
   
  }

  void loop()
  { 
    if (client.available() > 0) {
      char pin = client.read();
    }
}
  
  void setupEthernet()
  {
    Serial.println("Setting up Ethernet...");
    // start the Ethernet connection:
    Ethernet.begin(mac, ip);
    Serial.print("My IP address: ");
    Serial.println(Ethernet.localIP());
    delay(1000);
  
    client.connect(server, 8888);
    
    delay(1000);
  
  }
  
  
  

