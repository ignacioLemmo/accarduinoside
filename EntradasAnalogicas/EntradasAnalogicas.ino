   /*
    Telnet client
  
   This sketch connects to a a telnet server (http://www.google.com)
   using an Arduino Wiznet Ethernet shield.  You'll need a telnet server
   to test this with.
   Processing's ChatServer example (part of the network library) works well,
   running on port 10002. It can be found as part of the examples
   in the Processing application, available at
   http://processing.org/
  
   Circuit:
   * Ethernet shield attached to pins 10, 11, 12, 13
  
   created 14 Sep 2010
   modified 9 Apr 2012
   by Tom Igoe
  
   */
  
  #include <SPI.h>
  #include <Ethernet.h>
  //#include <TextFinder.h>
  byte mac[] = {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
  };
  //IP DE RED LOCAL
  IPAddress ip(192, 168, 0, 10);
  //IP DEL SERVIDOR NODE LOCAL
  IPAddress server(192,168,0, 3);
  
  //ID DE DISPOSITIVO OBTENIDO DE CUANDO SE CREA EL DISPOSITIVO
  int deviceID= 43;

int IA0 =0,IA1 =1,IA2 =2,IA3 =3, IA4 =4,IA5 =5; 

int valA0 =0,valA1 =0,valA2 =0,valA3 =0, valA4 =0,valA5 =0;

int readValA0=0,readValA1=0,readValA2=0,readValA3=0,readValA4=0,readValA5=0;

boolean isBusy=false;

int Lecturas[10];
int i = 0, Total = 0, Promedio = 0;
int valPot=0;

EthernetClient client; 

  void setup() {
    Serial.begin(9600);
   
   setupEthernet();
   
    if (client.connected()) {
  
      String data = "CONECT/";
     data.concat(deviceID); 
     client.print(data);
                 
     Serial.println("Conectado!");
        
        delay(1000);
      }
      
      else{
        
         Serial.println("CLIENTE NO DISPONIBLE!");           
       }   
  }
  
  void loop()
  { 
  valA2 = 0;
  valA3 = 0;
  
  valA2 = analogRead(IA2);

   if(readValA2 != valA2){

           String data = "ANALOG/";
     data.concat(deviceID);
     data.concat("/");
     data.concat(IA2);
     data.concat("/");
     data.concat(valA2); 
     client.print(data); 
     delay(100);
   }
   valA3 = analogRead(IA3);
   
   if(readValA3 != valA3){
           String data2 = "ANALOG/";
     data2.concat(deviceID);
     data2.concat("/");
     data2.concat(IA3);
     data2.concat("/");
     data2.concat(valA3); 
     client.print(data2); 
     delay(100);
   }
   readValA2=valA2;
   readValA3=valA3;

  }
  
  void setupEthernet()
  {
    Serial.println("Setting up Ethernet...");
    // start the Ethernet connection:
    Ethernet.begin(mac, ip);
    Serial.print("My IP address: ");
    Serial.println(Ethernet.localIP());
    delay(1000);
  
    client.connect(server, 8888);
    
    delay(1000);
  
  }
  
  
  

