#include "Arduino.h"
#include <Ethernet.h>
#include <SPI.h>
#include <WebSocketClient.h>

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
char server[] = "192.168.0.5:3001";
int port = 3000;
WebSocketClient client;

void setup() {
  Serial.begin(9600);
  Ethernet.begin(mac);
client.setDataArrivedDelegate(dataArrived);
  if (!client.connect(server)){ 
    Serial.println(F("Not connected."));
  }

  if (client.connected()) client.send("Client here!");
}

void loop() {
  client.monitor();
}

void dataArrived(WebSocketClient client, String data) {
  Serial.println("connected");
  Serial.println("Data Arrived: " + data);
}
