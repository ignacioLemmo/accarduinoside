  /*
    Telnet client
  
   This sketch connects to a a telnet server (http://www.google.com)
   using an Arduino Wiznet Ethernet shield.  You'll need a telnet server
   to test this with.
   Processing's ChatServer example (part of the network library) works well,
   running on port 10002. It can be found as part of the examples
   in the Processing application, available at
   http://processing.org/
  
   Circuit:
   * Ethernet shield attached to pins 10, 11, 12, 13
  
   created 14 Sep 2010
   modified 9 Apr 2012
   by Tom Igoe
  
   */
  
  #include <SPI.h>
  #include <Ethernet.h>
  //#include <TextFinder.h>
  byte mac[] = {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
  };
  //IP DE RED LOCAL
  IPAddress ip(192, 168, 1, 40);
  //IP DEL SERVIDOR NODE LOCAL
  IPAddress server(192,168,1,35);
  
  //ID DE DISPOSITIVO OBTENIDO DE CUANDO SE CREA EL DISPOSITIVO
  int deviceID= 13;
  
  int pins[8]={0,1,2,3,5,6,7,8};
  int remotePins[8]={'0','1','2','3','5','6','7','8'};
  char estados [8]={'L','L','L','L','L','L','L','L'};
  EthernetClient client; 
  //TextFinder  finder( client );
  void setup() {
    pinMode(0, OUTPUT);
    pinMode(1, OUTPUT);
    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(5, OUTPUT);
    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);
    pinMode(8, OUTPUT);
  
  
    
    Serial.begin(9600);
  
   setupEthernet();
  
  
    if (client.connected()) {
  
     client.print("CONECT/");
     client.print(deviceID);
          
              
      Serial.println("Conectado!");
        
        delay(1000);
      }
      
      else{
        
         Serial.println("CLIENTE NO DISPONIBLE!"); 
           
       }
   
  }

  void loop()
  { 
    if (client.available() > 0) {
      char pin = client.read();

      Serial.println(pin);
       for(int i=0;i<7;i++){

        if(pin==remotePins[i]){
          Serial.println("PIN");
            if(estados[i] == 'L'){
              estados[i]='H';
              digitalWrite(pins[i], HIGH);
              break;
              }
              else{
                  estados[i]='L';
                  digitalWrite(pins[i], LOW);
                  break;
              }
          }    
        
      }
    }
}
  
  void setupEthernet()
  {
    Serial.println("Setting up Ethernet...");
    // start the Ethernet connection:
    Ethernet.begin(mac, ip);
    Serial.print("My IP address: ");
    Serial.println(Ethernet.localIP());
    delay(1000);
  
    client.connect(server, 8888);
    
    delay(1000);
  
  }
  
  
  

