#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN 8
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);

MFRC522::MIFARE_Key key;
byte nuidPICC[3];


  #include <Ethernet.h>
  int deviceID= 30;
  //#include <TextFinder.h>
  byte mac[] = {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
  };
  //IP DE RED LOCAL
  IPAddress ip(192, 168, 0, 34);
  //IP DEL SERVIDOR NODE LOCAL
  IPAddress server(192,168,0, 4);
EthernetClient client; 
void setup() {
    Serial.begin(9600);
    SPI.begin();
     setupEthernet();
   
    if (client.connected()) {
  
     client.print("CONECT/");
     client.print(deviceID);
                 
     Serial.println("Conectado!");
        
        delay(1000);
      }
      
      else{
        
         Serial.println("CLIENTE NO DISPONIBLE!");           
       }

                        // Init SPI bus
               // Init MFRC522
mfrc522.PCD_Init(); 
for (byte i = 0; i < 6; i++)
   { key.keyByte[i] = 0xDD;
   }
dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);
}

void loop() {
  if ( ! mfrc522.PICC_IsNewCardPresent())
       
       return;

if ( ! mfrc522.PICC_ReadCardSerial())
       return;
    Serial.print(F("Card UID:"));
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    // Check for compatibility
    if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
        Serial.println(F("This sample only works with MIFARE Classic cards."));
        return;
    }


    if (mfrc522.uid.uidByte[0] != nuidPICC[0] ||
    mfrc522.uid.uidByte[1] != nuidPICC[1] ||
    mfrc522.uid.uidByte[2] != nuidPICC[2] ||
    mfrc522.uid.uidByte[3] != nuidPICC[3] )
     {
        Serial.println(F("A new card has been detected."));
        Serial.println(F("The NUID tag is:"));
        Serial.print(F("In hex: "));
        client.print("TAG READ");
        dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
        Serial.println();

        mfrc522.PICC_HaltA();
        mfrc522.PCD_StopCrypto1();
    }
}

void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}
  void setupEthernet()
  {
    Serial.println("Setting up Ethernet...");
    // start the Ethernet connection:
    Ethernet.begin(mac, ip);
    Serial.print("My IP address: ");
    Serial.println(Ethernet.localIP());
    delay(1000);
  
    client.connect(server, 8888);
    
    delay(1000);
  
  }


